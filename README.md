# Recipient Replacer

Replacing the email recipient in the local environment

#### Install
```
composer require tsyvkunov/recipientreplacer
```

#### Config Files

```
php artisan vendor:publish --provider="Tsyvkunov\RecipientReplacer\RecipientReplacerServiceProvider"
```
