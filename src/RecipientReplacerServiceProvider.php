<?php

namespace Tsyvkunov\RecipientReplacer;

use Illuminate\Mail\Events\MessageSending;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class RecipientReplacerServiceProvider extends ServiceProvider
{

    protected $listen = [
        MessageSending::class => [
            'Tsyvkunov\RecipientReplacer\RecipientReplacer',
        ],
    ];

    public function boot()
    {
        parent::boot();
        $this->publishes([
            __DIR__.'/../config/recipientreplacer.php' => config_path('recipientreplacer.php'),
        ]);
    }
}
