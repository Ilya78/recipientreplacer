<?php
namespace Tsyvkunov\RecipientReplacer;

use Illuminate\Mail\Events\MessageSending;
use Illuminate\Support\Facades\App;

class RecipientReplacer
{
    /*
     *
     */
    public function handle(MessageSending $sending): void
    {
        if(App::environment() !== 'production') {
            $msg = $sending->message;
            if(!empty(config('recipientreplacer.email')))
                $msg->setTo(config('recipientreplacer.email'));
        }
    }
}

